package com.testspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.testspring.dto.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
