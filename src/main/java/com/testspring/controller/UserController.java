package com.testspring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testspring.customerror.ResourceNotFoundException;
import com.testspring.dto.Department;
import com.testspring.dto.User;
import com.testspring.dto.UserListItem;
import com.testspring.repository.DepartmentRepository;
import com.testspring.repository.UserRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DepartmentRepository departmentRepository;

	@GetMapping("/user/v2")
	public List<UserListItem> getAllUserV2() {
		List<UserListItem> resultList = new ArrayList<>();
		List<User> tempList = userRepository.findAll();
		tempList.forEach(user -> {
			int department_id = user.getDepartment_id();
			try {
				Department tempDepartment = departmentRepository.findById(department_id).orElseThrow(() -> new ResourceNotFoundException("Department" + department_id + " not found"));
				resultList.add(new UserListItem(user.getId(), user.getName(), user.getBirthday(), user.getDepartment_id(), tempDepartment.getName(), user.getMemo()));
			} catch (ResourceNotFoundException e) {
				e.printStackTrace();
			}
		});
		return resultList;
	}

	@GetMapping("/user/v1")
	public List<User> getAllUserV1() {
		return userRepository.findAll();
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<User> getUserByID(@PathVariable(value = "id") Integer userID) throws ResourceNotFoundException {
		User user = userRepository.findById(userID).orElseThrow(() -> new ResourceNotFoundException("User " + userID + " not found"));

		return ResponseEntity.ok().body(user);
	}

	@PostMapping("/user")
	public User createUser(@Valid @RequestBody User user) {
			return userRepository.save(user);
	}

	@PutMapping("/user/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Integer userID,
			@Valid @RequestBody User updatedUser) throws Exception {
		User tempUser = userRepository.findById(userID).orElseThrow(() -> new ResourceNotFoundException("User " + userID + " not found"));

		tempUser.setName(updatedUser.getName());
		tempUser.setBirthday(updatedUser.getBirthday());
		tempUser.setDepartment_id(updatedUser.getDepartment_id());
		tempUser.setMemo(updatedUser.getMemo());

		final User update = userRepository.save(tempUser);

		return ResponseEntity.ok(update);
	}

	@DeleteMapping("/user/{id}")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Integer userID) throws Exception {
		User user = userRepository.findById(userID).orElseThrow(() -> new ResourceNotFoundException("User " + userID + " not found"));

		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);

		return response;
	}
}
