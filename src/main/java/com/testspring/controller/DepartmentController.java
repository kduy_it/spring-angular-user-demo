package com.testspring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testspring.customerror.ResourceNotFoundException;
import com.testspring.dto.Department;
import com.testspring.repository.DepartmentRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class DepartmentController {

	@Autowired
	private DepartmentRepository departmentRepository;

	@GetMapping("/department")
	public List<Department> getDepartmentList() {
		return departmentRepository.findAll();
	}

	@GetMapping("/department/{id}")
	public ResponseEntity<Department> getDepartmentById(@PathVariable(value = "id") Integer departmentID) throws ResourceNotFoundException {
		Department department = departmentRepository.findById(departmentID).orElseThrow(() -> new ResourceNotFoundException("Could not find department " + departmentID));
		return ResponseEntity.ok().body(department);
	}
}
