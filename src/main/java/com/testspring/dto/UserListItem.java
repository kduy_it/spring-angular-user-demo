package com.testspring.dto;

public class UserListItem {
	private int id;
	private String user_name;
	private String birthday;
	private int department_id;
	private String department_name;
	private String memo;

	public UserListItem() {

	}

	public UserListItem(int id, String user_name, String birthday, int department_id, String department_name, String memo) {
		this.id = id;
		this.user_name = user_name;
		this.birthday = birthday;
		this.department_id = department_id;
		this.department_name = department_name;
		this.memo = memo;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
}
